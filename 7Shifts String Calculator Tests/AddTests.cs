﻿using System;
using _7Shifts_String_Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

/// <summary>
/// By Brett Hickie
/// </summary>
namespace _7Shifts_String_Calculator_Tests
{
    [TestClass]
    public class AddTests
    {
        #region Step 1 Tests
        /// <summary>
        /// This test checks for basic, comma separated lists of numbers
        /// </summary>
        [TestMethod]
        public void TestStep1Inputs()
        {
            Assert.AreEqual(0, Program.Add(""));

            Assert.AreEqual(8, Program.Add("1,2,5"));
        }

        /// <summary>
        /// This test checks for invalid inputs with no commas throwing an exception
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void TestStep1InvalidInput1()
        {
            Program.Add("Testing");
        }

        /// <summary>
        /// This test checks for invalid inputs with commas throwing an exception
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void TestStep1InvalidInput2()
        {
            Program.Add("Testing,with,comma");
        }
        #endregion

        #region Step 2 Tests
        /// <summary>
        /// This test ensures that unnecessary \n characters are filtered out
        /// </summary>
        [TestMethod]
        public void TestStep2Inputs()
        {
            Assert.AreEqual(6, Program.Add("1\n,2,3"));

            Assert.AreEqual(7, Program.Add("1,\n2,4"));
        }
        #endregion

        #region Step 3 Tests
        /// <summary>
        /// This test checks for custom, one character delimiters
        /// </summary>
        [TestMethod]
        public void TestStep3Inputs()
        {
            Assert.AreEqual(6, Program.Add("//$\n1$2$3"));

            Assert.AreEqual(13, Program.Add("//@\n2@3@8"));

            Assert.AreEqual(8, Program.Add("1,2,5"));
        }

        /// <summary>
        /// This test checks for malformed one character delimiters throwing an exception
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void TestStep3InvalidInput1()
        {
            Program.Add("/$\n1$2$3");
        }

        /// <summary>
        /// This test checks that other characters (not related to a custom delimiter declaration) at the start of the string throws an exception
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void TestStep3InvalidInput2()
        {
            Program.Add("s1$2$3");
        }
        #endregion

        #region Step 4 Tests
        /// <summary>
        /// This test checks to ensure negative numbers throw an exception
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void TestStep4Input()
        {
            Program.Add("1\n,-2,3");
        }
        #endregion

        #region Bonus 1 Tests
        /// <summary>
        /// This test checks that numbers greater than 1000 are ignored (but otherwise the calculator functions normally)
        /// </summary>
        [TestMethod]
        public void TestBonus1Input()
        {
            Assert.AreEqual(2, Program.Add("2,1001"));

            Assert.AreEqual(8, Program.Add("2,3246,1,5,1842"));
        }
        #endregion

        #region Bonus 2 Tests
        /// <summary>
        /// This test checks that custom delimiters can be any length
        /// </summary>
        [TestMethod]
        public void TestBonus2Input()
        {
            Assert.AreEqual(6, Program.Add("//***\n1***2***3"));

            Assert.AreEqual(9, Program.Add("//^^\n2^^4^^3"));
        }
        #endregion

        #region Bonus 3 Tests
        /// <summary>
        /// This test checks that multiple delimiters can be used
        /// </summary>
        [TestMethod]
        public void TestBonus3Input()
        {
            Assert.AreEqual(6, Program.Add("//$,@\n1$2@3"));

            Assert.AreEqual(10, Program.Add("//!,@,#\n4#3@2!1"));
        }
        #endregion

        #region Bonus 4 Tests
        /// <summary>
        /// This test checks that multiple delimiters of any length can be used
        /// </summary>
        [TestMethod]
        public void TestBonus4Input()
        {
            Assert.AreEqual(13, Program.Add("//@@,##\n3@@4##6"));

            Assert.AreEqual(9, Program.Add("//!@,#$\n2!@3#$4"));
        }
        #endregion
    }
}
