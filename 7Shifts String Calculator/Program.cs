﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

/// <summary>
/// By Brett Hickie
/// </summary>
namespace _7Shifts_String_Calculator
{
    public class Program
    {
        /// <summary>
        /// This method will take in a string of numbers separated by a delimiter (which can specified in the way of //(delimiter)\n at the start of the string, else comma) and add them
        /// </summary>
        /// <param name="numbers">string of numbers separated by a delimiter (which can specified in the way of //(delimiter)\n at the start of the string, else comma)</param>
        /// <returns>The sum of the numbers</returns>
        public static int Add(string numbers)
        {
            int returnVal = 0;

            //Checking for empty string
            if (numbers != "")
            {
                String[] numbersToParse = splitNumbers(numbers);

                //Looping through the array and adding the numbers together
                for (int i = 0; i < numbersToParse.Length; i++)
                {
                    int val = int.Parse(numbersToParse[i]);

                    //Checking to see if val is negative
                    if(val < 0)
                    {
                        //val is negative, throw exception
                        throw new NotSupportedException(String.Format("Negatives not allowed: %n is an invalid number", val));
                    }

                    //Checking to see if val is greater than 1000
                    if(val > 1000)
                    {
                        //val is greater than 1000, ignore this value
                        continue;
                    }

                    returnVal += val;
                }
            }

            return returnVal;
        }


        /// <summary>
        /// This method will split the numbers string based on the delimiter
        /// </summary>
        /// <param name="numbers">String optinally starting with custom delimiter (indicated by //(delimiter)\n) or comma if none provided and followed by numbers separated by delimiter</param>
        /// <returns>An array containing the separated numbers as strings</returns>
        private static String[] splitNumbers(string numbers)
        {
            String[] returnArray;

            string singleDelimiter;
            string[] multiDelimiter = new string[] { };

            bool multiDelimCheck = false;

            //Separate out custom delimeter (if any)

            //Custom delimeter must start with //
            if (numbers.Substring(0, 2).Equals("//"))
            {
                //Custom delimeter must end with \n, so checking if we have one
                if(numbers.Contains('\n'))
                {
                    //Valid delimeter syntax exists cutting it from string and storing separately

                    //First, removing // at start of string
                    numbers = numbers.Substring(2);

                    //Next, taking all characters from the start of the new string (right after the //) to the first \n character
                    singleDelimiter = numbers.Substring(0, numbers.IndexOf('\n'));

                    //Checking if multiple delimiters are being used
                    if(singleDelimiter.IndexOf(',') != -1)
                    {
                        //Multiple delimiters are being used
                        multiDelimCheck = true;

                        multiDelimiter = singleDelimiter.Split(',');
                    }

                    //Lastly, removing the custom delimiter and the \n character that marks the end of the custom delimiter
                    numbers = numbers.Substring(numbers.IndexOf('\n') + 1);
                }
                else
                {
                    //String does not begin with a proper custom delimiter, throw exception
                    throw new FormatException("Invalid custom delimiter syntax (should be //<delimiter>\\n)");
                }
            }
            else
            {
                //If there is no custom delimeter, check to see that the string begins with a number
                if(Regex.IsMatch(numbers[0].ToString(), "[0-9]"))
                {
                    //String begins with a number, so we will assume a comma delimited list (if it is not a comma delimited list, it will get sorted out by other checks later)
                    singleDelimiter = ",";
                }
                else
                {
                    //String does not begin with a number or a proper custom delimiter, throw exception
                    throw new FormatException("Invalid custom delimiter syntax (should be //<delimiter>\\n)");
                }
            }


            //Removing unnecessary \n commands from the string
            numbers = numbers.Replace("\n", "");

            //Splitting the numbers array needs to be handled differently depending on whether or not we are using one or multiple delimiters
            if (multiDelimCheck)
            {
                returnArray = numbers.Split(multiDelimiter, StringSplitOptions.None);
            }
            else
            {
                returnArray = numbers.Split(new string[] { singleDelimiter }, StringSplitOptions.None);
            }

            return returnArray;
        }

        static void Main(string[] args)
        {
            Console.Write("Enter numbers separated by a ,\n");
            String readVal = Console.ReadLine();

            Console.Write("The numbers add to %n", Add(readVal));
        }
    }
}
